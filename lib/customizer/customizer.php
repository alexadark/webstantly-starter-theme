<?php
/**
 * Webstantly theme customizer
 *
 * @package Webstantly Starter theme
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function wst_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'        => '.site-title a',
			'render_callback' => 'wst_customize_partial_blogname',
		) );
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector'        => '.site-description',
			'render_callback' => 'wst_customize_partial_blogdescription',
		) );

	}

}

add_action( 'customize_register', 'wst_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function wst_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function wst_customize_partial_blogdescription() {
	bloginfo( 'description' );
}



/**
 * Add the theme configuration
 */
wstCustomizer::add_config( 'webstantly_theme', array(
	'option_type' => 'theme_mod',
	'capability'  => 'edit_theme_options',
) );

include_once 'customizer-modules/customizer-options.php';
include_once 'customizer-modules/customizer-logo.php';

if (get_theme_mod('enable_colors')) {
	include_once 'customizer-modules/customizer-colors.php';
}

if (get_theme_mod('enable_colors')) {

	include_once 'customizer-modules/customizer-typography.php';
}

include_once 'customizer-modules/customizer-header.php';

include_once 'customizer-modules/customizer-post.php';

if (get_theme_mod('enable_widths')) {

	include_once 'customizer-modules/customizer-width.php';
}










