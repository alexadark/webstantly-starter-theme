<?php 
/*-----------------------------------------------------------
	CUSTOMIZER OPTIONS
/*------------------------------------------------------------*/
wstCustomizer::add_section( 'options', array(
	'title'      => esc_attr__( 'Customizer Options', CHILD_TEXT_DOMAIN ),
	'priority'   => 1,
	'description' => esc_attr__( 'Enable the options you are interested in, then click save and publish, close the customizer and reopen it in order to use these options' ),
	'capability' => 'edit_theme_options',
) );
wstCustomizer::add_field( 'webstantly_theme', array(
	'type'        => 'switch',
	'settings'    => 'enable_colors',
	'label'       => esc_attr__( 'Activate Color Options', CHILD_TEXT_DOMAIN ),
	'description' => esc_attr__( 'here you can enable colors in the customizer' ),
	'section'     => 'options',
	'choices'     => array(
		'on'  => esc_attr__( 'Enabled', 'textdomain' ),
		'off' => esc_attr__( 'Disabled', 'textdomain' ),
	),
	'default'     => 0 ,

	));

wstCustomizer::add_field( 'webstantly_theme', array(
	'type'        => 'switch',
	'settings'    => 'enable_typo',
	'label'       => esc_attr__( 'Enable Typography Options', CHILD_TEXT_DOMAIN ),
	'description' => esc_attr__( 'Here you can enable typography options in the customizer' ),
	'section'     => 'options',
	'choices'     => array(
		'on'  => esc_attr__( 'Enabled', 'textdomain' ),
		'off' => esc_attr__( 'Disabled', 'textdomain' ),
	),
	'default'     => 0,
	
	));
wstCustomizer::add_field( 'webstantly_theme', array(
	'type'        => 'switch',
	'settings'    => 'enable_widths',
	'label'       => esc_attr__( 'Enable Width Options', CHILD_TEXT_DOMAIN ),
	'description' => esc_attr__( 'Here you can enable the widths in the customizer' ),
	'section'     => 'options',
	'choices'     => array(
		'on'  => esc_attr__( 'Enabled', 'textdomain' ),
		'off' => esc_attr__( 'Disabled', 'textdomain' ),
	),
	'default'     => 0,
	
	));