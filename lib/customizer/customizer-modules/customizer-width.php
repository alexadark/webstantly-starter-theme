<?php
/*-----------------------------------------------------------
LAYOUT
/*------------------------------------------------------------*/

wstCustomizer::add_section( 'width', array(
'title'      => esc_attr__( 'Widths', CHILD_TEXT_DOMAIN ),
'priority'   => 1,
'capability' => 'edit_theme_options',
) );

wstCustomizer::add_field( 'webstantly_theme', array(
'type'        => 'slider',
'settings'    => 'layout_width',
'label'       => esc_attr__( 'Layout Width', CHILD_TEXT_DOMAIN ),
'description' => esc_attr__( 'Define the width of your layout' ),
'section'     => 'width',
'transport'   => 'auto',
'choices'     => array(
'min' => '800',
'max' => '1450'
),
'default'     => 1200,
'output'      => array(
array(
'element'  => '.uk-container, .wrap',
'property' => 'max-width',
'units'    => 'px',
),
),

) );