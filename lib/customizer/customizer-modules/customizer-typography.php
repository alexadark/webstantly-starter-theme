<?php
/*-----------------------------------------------------------
TYPOGRAPHY
/*------------------------------------------------------------*/


/**
 * Add the typography section
 */
wstCustomizer::add_section( 'typography', array(
	'title'      => esc_attr__( 'Typography', CHILD_TEXT_DOMAIN ),
	'priority'   => 2,
	'capability' => 'edit_theme_options',
) );

/**
 * Add the body-typography control
 */
wstCustomizer::add_field( 'webstantly_theme', array(
	'type'        => 'typography',
	'settings'    => 'body_typography',
	'label'       => esc_attr__( 'Body Typography', CHILD_TEXT_DOMAIN ),
	'description' => esc_attr__( 'Select the main typography options for your site.', CHILD_TEXT_DOMAIN ),
	'help'        => esc_attr__( 'The typography options you set here apply to all content on your site.', CHILD_TEXT_DOMAIN ),
	'section'     => 'typography',
	'transport'   => 'auto',
	'priority'    => 10,
	'default'     => array(
		'font-family'    => 'Raleway',
		'variant'        => '400',
		'font-size'      => '16px',
		'line-height'    => '1.5',
		'letter-spacing' => '0',
		'color'          => '#333333',
	),

	'output' => array(
		array(
			'element' => 'body',
		),
	),
) );

/**
 * Add the header-typography control
 */
wstCustomizer::add_field( 'webstantly_theme', array(
	'type'        => 'typography',
	'settings'    => 'headings_typography',
	'label'       => esc_attr__( 'Headings Typography', CHILD_TEXT_DOMAIN ),
	'description' => esc_attr__( 'Select the typography options for your headers.', CHILD_TEXT_DOMAIN ),
	'help'        => esc_attr__( 'The typography options you set here will override the Body Typography options for all headers on your site (post titles, widget titles etc).', CHILD_TEXT_DOMAIN ),
	'section'     => 'typography',
	'transport'   => 'auto',
	'priority'    => 11,
	'default'     => array(
		'font-family'    => 'Raleway',
		'variant'        => '400',
//		 'font-size'      => '16px',
		'line-height'    => '1.5',
		'letter-spacing' => '0',
		'color'          => '#333333',
		'text-transform' => 'none',
	),

	'output' => array(
		array(
			'element' => array(
				'h1',
				'h2',
				'h3',
				'h4',
				'h5',
				'h6',
				'.h1',
				'.h2',
				'.h3',
				'.h4',
				'.h5',
				'.h6',
				'h1 a',
				'h2 a',
				'h3 a',
				'h4 a',
				'h5 a',
				'h6 a',
				'.entry-title a',
				'.site-title a'
			),
		),
	),
) );

/**
 * Navigation typography
 */
wstCustomizer::add_field( 'webstantly_theme', array(
	'type'        => 'typography',
	'settings'    => 'vav_typography',
	'label'       => esc_attr__( 'Navigation Typography', CHILD_TEXT_DOMAIN ),
	'description' => esc_attr__( 'Select the navigations typography options for your site.', CHILD_TEXT_DOMAIN ),
	'help'        => esc_attr__( 'The typography options you set here apply to all navigations on your site.', CHILD_TEXT_DOMAIN ),
	'section'     => 'typography',
	'transport'   => 'auto',
	'priority'    => 12,
	'default'     => array(
		'font-family'    => 'Raleway',
		'variant'        => '700',
		'font-size'      => '11px',
		'line-height'    => '1.5',
		'letter-spacing' => '1',
		'color'          => '#333333',
		'text-transform' => 'uppercase',
	),

	'output' => array(
		array(
			'element' => '.genesis-nav-menu a',
		),
	),
) );
