<?php
/*-----------------------------------------------------------
POST
/*------------------------------------------------------------*/

wstCustomizer::add_section( 'posts', array(
	'title'      => esc_attr__( 'Blog', CHILD_TEXT_DOMAIN ),
	'priority'   => 3,
	'capability' => 'edit_theme_options',
) );

wstCustomizer::add_field( 'webstantly_theme', array(
	'type'        => 'toggle',
	'settings'    => 'post_shadow',
	'label'       => esc_attr__( 'Post Box Shadow', CHILD_TEXT_DOMAIN ),
	'description' => esc_attr__( 'disable to remove box shadow on posts' ),
	'section'     => 'posts',
//	'transport'=>'postMessage',
	'default'     => 1,
//	'js_vars' => array(
//		array(
//			'element' => '.entry',
//			'property'=> 'box-shadow',
//
//		)
//	)
//'output'=> array(
//	array(
//		'element' => '.entry.shadow'
//	)
//)

) );

wstCustomizer::add_field( 'webstantly_theme', array(
	'type'        => 'toggle',
	'settings'    => 'post_info',
	'label'       => esc_attr__( 'Post Info', CHILD_TEXT_DOMAIN ),
	'description' => esc_attr__( 'Disable to remove post info' ),
	'section'     => 'posts',
//	'transport'=>'postMessage',
	'default'     => 1,
//	'js_vars' => array(
//		array(
//			'element' => '.entry-header .entry-meta',
//			'property'=> 'display',
//
//		)
//	)
//'output'=> array(
//	array(
//		'element' => '.entry.shadow'
//	)
//)

) );


if ( get_theme_mod( 'post_shadow', true ) ) {
	add_filter( 'genesis_attr_entry', 'wst_change_entry_attr', 99 );
	function wst_change_entry_attr( $attr ) {
		$attr['class'] .= ' shadow';

		return $attr;
	}

	add_filter( 'genesis_attr_sidebar-primary', 'wst_change_sidebar_shadow_attr', 99 );
	function wst_change_sidebar_shadow_attr( $attr ) {
		$attr['class'] .= ' shadow';

		return $attr;
	}
}

add_action( 'wp', function () {
	if ( get_theme_mod( 'post_info', true ) ) {
		return;
	}
	remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );
} );

Kirki::add_field( 'webstantly_theme', array(
	'type'     => 'select',
	'settings' => 'blog_layout',
	'label'    => esc_html__( 'Blog Layout', CHILD_TEXT_DOMAIN ),
	'section'  => 'posts',
	'default'  => 'content-sidebar',
	'priority' => 10,
	'choices'  => array(
		'content_sidebar' => esc_attr__( 'Content Sidebar', CHILD_TEXT_DOMAIN ),
		'sidebar_content' => esc_attr__( 'Sidebar Content', CHILD_TEXT_DOMAIN ),
		'grid_2'  => esc_attr__( 'Grid 2 Columns', CHILD_TEXT_DOMAIN ),
		'grid_3'  => esc_attr__( 'Grid 3 Columns', CHILD_TEXT_DOMAIN ),

	),
) );



