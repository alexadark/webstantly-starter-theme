<?php
wstCustomizer::add_section( 'logo', array(
	'title'      => esc_attr__( 'Logo', CHILD_TEXT_DOMAIN ),
	'priority'   => 1,
	'capability' => 'edit_theme_options',
) );

wstCustomizer::add_field( 'webstantly_theme', array(
	'type'        => 'image',
	'settings'    => 'logo_image',
	'label'       => __( 'Logo Image', CHILD_TEXT_DOMAIN ),
	'description' => __( 'Upload your logo', CHILD_TEXT_DOMAIN ),
	'section'     => 'logo',
	'default'     => '',
	'transport'=> 'auto',
	'priority'    => 10,
	
) );

wstCustomizer::add_field( 'webstantly_theme', array(
	'type'        => 'switch',
	'settings'    => 'site_description',
	'label'       => esc_attr__( 'Enable Site Description', CHILD_TEXT_DOMAIN ),
	'description' => esc_attr__( '' ),
	'section'     => 'logo',
	'choices'     => array(
		'on'  => esc_attr__( 'Enabled', 'textdomain' ),
		'off' => esc_attr__( 'Disabled', 'textdomain' ),
	),
	'default'     => 1,
	
	));

if(!get_theme_mod('site_description',1)){
	remove_action('genesis_site_description', 'genesis_seo_site_description');
}

//wstCustomizer::add_field( 'webstantly_theme', array(
//	'type'        => 'switch',
//	'settings'    => 'description_below',
//	'label'       => esc_attr__( 'Description Below logo or title', CHILD_TEXT_DOMAIN ),
//	'description' => esc_attr__( 'if enabled the description will be below the logo' ),
//	'section'     => 'logo  ',
//	'choices'     => array(
//		'on'  => esc_attr__( 'Enabled', 'textdomain' ),
//		'off' => esc_attr__( 'Disabled', 'textdomain' ),
//	),
//	'default'     => 0 ,
//	'transport'=> 'auto',
//	'output' => array(
//		array(
//			'element'=> '.title-area',
//			'property'=> 'flex-direction, -ms-flex-direction',
//			'value_pattern' => 'column'
//		),
//		array(
//			'element'=> '.title-area',
//			'property'=> '-webkit-box-orient',
//			'value_pattern' => 'vertical'
//		),
//		array(
//			'element'=> '.title-area',
//			'property'=> '-webkit-box-direction',
//			'value_pattern' => 'normal'
//		),
//
//	)
//
//
//	));