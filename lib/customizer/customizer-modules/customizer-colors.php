<?php
/*-----------------------------------------------------------
COLORS
/*------------------------------------------------------------*/
/**
 * Add the Color section
 */
wstCustomizer::add_section( 'colors', array(
	'title'      => esc_attr__( 'Colors', CHILD_TEXT_DOMAIN ),
	'priority'   => 2,
	'capability' => 'edit_theme_options',
) );

wstCustomizer::add_field( 'webstantly_theme', array(
	'type'      => 'color',
	'alpha'     => true,
	'settings'  => 'primary_color',
	'label'     => __( 'Primary Color', CHILD_TEXT_DOMAIN ),
	'section'   => 'colors',
	'default'   => '#E91E63',
	'priority'  => 10,
	'transport' => 'auto',
	'output'    => array(
		array(
			'element'  => '.uk-section-primary, .spacer, .uk-tile-primary, .uk-card-primary, .uk-alert-primary, .uk-text-background, .uk-background-primary, .enews-widget input, .archive-pagination li a:hover, .woocommerce-pagination li a:hover, .archive-pagination .active a, .woocommerce-pagination .active a, span.onsale, .woocommerce-pagination .current, .wc-tabs .active-border, .uk-radio:checked:focus,.uk-checkbox:checked:focus,.uk-checkbox:indeterminate:focus, .uk-badge, .uk-alert-primary, ins'
		,
			'property' => 'background-color'
		),
		array(
			'element'  => ' em, .genesis-nav-menu a:focus,.genesis-nav-menu a:hover,.genesis-nav-menu .current-menu-item > a,.genesis-nav-menu .sub-menu .current-menu-item > a:focus,.genesis-nav-menu .sub-menu .current-menu-item > a:hover, .nav-primary .genesis-nav-menu a:hover,.nav-primary .genesis-nav-menu .current-menu-item > a,.nav-primary .genesis-nav-menu .sub-menu .current-menu-item > a:hover, .menu-toggle:focus,.menu-toggle:hover,.sub-menu-toggle:focus,.sub-menu-toggle:hover, .added_to_cart, .product-remove a  '
		,
			'property' => 'color'
		),
		array(
			'element'  => '.uk-radio:focus,.uk-checkbox:focus, .uk-form-label, .uk-card-default .uk-card-title, .uk-totop:active, .woocommerce-error,.woocommerce-info,.woocommerce-message, .woocommerce table.shop_table'
		,
			'property' => 'border-color'
		),
		array(
			'element'  => '.photo-text:nth-child(odd) .photo-text-content.uk-section-primary:after, .photo-text:nth-child(even) .photo-text-content.uk-section-primary:before '
		,
			'property' => 'border-left-color'
		),
		array(
			'element'  => '.photo-text:nth-child(odd) .photo-text-content.uk-section-primary:after, .photo-text:nth-child(even) .photo-text-content.uk-section-primary:before '
		,
			'property' => 'border-right-color'
		),


	),
) );

wstCustomizer::add_field( 'webstantly_theme', array(
	'type'      => 'color',
	'alpha'     => true,
	'settings'  => 'background_color',
	'label'     => __( 'Background Color', CHILD_TEXT_DOMAIN ),
	'section'   => 'colors',
	'default'   => '#f5f5f5',
	'priority'  => 9,
	'transport' => 'auto',
	'output'    => array(
		array(
			'element'  => '.site-inner, html'
		,
			'property' => 'background-color'
		),
	),
) );

wstCustomizer::add_field( 'webstantly_theme', array(
	'type'      => 'color',
	'alpha'     => true,
	'settings'  => 'secondary_color',
	'label'     => __( 'Secondary Color', CHILD_TEXT_DOMAIN ),
	'section'   => 'colors',
	'default'   => '#2D3047',
	'priority'  => 11,
	'transport' => 'auto',
	'output'    => array(
		array(
			'element'  => '.uk-section-secondary, .uk-background-secondary, uk-tile-secondary, .uk-card-secondary, .uk-card-secondary.uk-card-hover:hover'
		,
			'property' => 'background-color'
		),
		array(
			'element'  => '.entry-title a:hover, .entry-title a:visited, .entry-title a:focus, .entry-title a:active'
		,
			'property' => 'color'
		),
		array(
			'element'  => ' .photo-text:nth-child(odd) .photo-text-content.uk-section-secondary:after, .photo-text:nth-child(even) .photo-text-content.uk-section-secondary:before '
		,
			'property' => 'border-left-color'
		),
		array(
			'element'  => '.photo-text:nth-child(odd) .photo-text-content.uk-section-secondary:after, .photo-text:nth-child(even) .photo-text-content.uk-section-secondary:before '
		,
			'property' => 'border-right-color'
		),
	),
) );

wstCustomizer::add_field( 'webstantly_theme', array(
	'type'      => 'color',
	'alpha'     => true,
	'settings'  => 'muted_color',
	'label'     => __( 'Muted Color', CHILD_TEXT_DOMAIN ),
	'section'   => 'colors',
	'default'   => '#f5f5f5',
	'priority'  => 12,
	'transport' => 'auto',
	'output'    => array(
		array(
			'element'  => '.uk-section-muted, .uk-background-muted'
		,
			'property' => 'background-color'
		),
		array(
			'element'  => '.photo-text:nth-child(odd) .photo-text-content.uk-section-muted:after, .photo-text:nth-child(even) .photo-text-content.uk-section-muted:before '
		,
			'property' => 'border-left-color'
		),
		array(
			'element'  => '.photo-text:nth-child(odd) .photo-text-content.uk-section-muted:after, .photo-text:nth-child(even) .photo-text-content.uk-section-muted:before '
		,
			'property' => 'border-right-color'
		),

	),
) );

// Add Fields.
wstCustomizer::add_field( 'webstantly_theme', array(
	'type'      => 'color',
	'alpha'     => true,
	'settings'  => 'color_top',
	'label'     => esc_attr__( 'Gradient First Color', CHILD_TEXT_DOMAIN ),
	'section'   => 'colors',
	'default'   => 'rgba(233, 30, 99, 0.8)',
	'priority'  => 13,
	'output'    => array(
		array(
			'element'         => '.uk-overlay-gradient',
			'property'        => 'background',
			'value_pattern'   => 'linear-gradient(angledeg, $ 30%,bottomCol 80%)',
			'pattern_replace' => array(
				'bottomCol' => 'color_bottom',
				'angle'     => 'color_angle',

			),
		),
	),
) );

wstCustomizer::add_field( 'webstantly_theme', array(
	'type'     => 'color',
	'alpha'    => true,
	'settings' => 'color_bottom',
	'label'    => esc_attr__( 'Gradient Second Color', CHILD_TEXT_DOMAIN ),
	'section'  => 'colors',
	'default'  => 'rgba(45, 49, 71, 0.8)',
	'priority' => 14,
	'output'   => array(
		array(
			'element'         => '.uk-overlay-gradient',
			'property'        => 'background',
			'value_pattern'   => 'linear-gradient(angledeg, topCol 30%,$ 80%)',
			'pattern_replace' => array(
				'topCol' => 'color_top',
				'angle'  => 'color_angle',
			),
		),
	),
) );

wstCustomizer::add_field( 'webstantly_theme', array(
	'type'      => 'slider',
	'settings'  => 'color_angle',
	'label'     => esc_attr__( 'Gradient Angle', CHILD_TEXT_DOMAIN ),
	'section'   => 'colors',
	'default'   => 135,
	'priority'  => 15,
	'choices'   => array(
		'min'  => -360,
		'max'  => 360,
		'step' => 1,
	),
	'output'    => array(
		array(
			'element'         => '.uk-overlay-gradient',
			'property'        => 'background',
			'value_pattern'   => 'linear-gradient($deg, topCol 30%,bottomCol 80%)',
			'pattern_replace' => array(
				'topCol'    => 'color_top',
				'bottomCol' => 'color_bottom',
			),
		),
	),
) );

wstCustomizer::add_field( 'webstantly_theme', array(
	'type'      => 'color',
	'alpha'     => true,
	'description'=> __('This color will also be used for links', CHILD_TEXT_DOMAIN),
	'settings'  => 'primary_button_color',
	'label'     => __( 'Primary Button Background Color', CHILD_TEXT_DOMAIN ),
	'section'   => 'colors',
	'default'   => '#E91E63',
	'priority'  => 16,
	'transport' => 'auto',
	'output'    => array(
		array(
			'element'  => '.uk-button-primary'
		,
			'property' => 'background-color'
		),
		array(
			'element'  => 'a:not(.uk-button)'
		,
			'property' => 'color'
		),
	),
) );

wstCustomizer::add_field( 'webstantly_theme', array(
	'type'      => 'color',
	'alpha'     => true,
	'description'=> __('This color will also be used for hover links', CHILD_TEXT_DOMAIN),
	'settings'  => 'primary_button_hover_color',
	'label'     => __( 'Primary Button Background Hover Color', CHILD_TEXT_DOMAIN ),
	'section'   => 'colors',
	'default'   => '#E91E63',
	'priority'  => 16,
	'transport' => 'auto',
	'output'    => array(
		array(
			'element'  => '.uk-button-primary:hover'
		,
			'property' => 'background-color'
		),
		array(
			'element'  => 'a:hover:not(.uk-button)'
		,
			'property' => 'color'
		),
	),
) );

wstCustomizer::add_field( 'webstantly_theme', array(
	'type'      => 'color',
	'alpha'     => true,
	'description'=> __('This color will also be used for links', CHILD_TEXT_DOMAIN),
	'settings'  => 'primary_button_color',
	'label'     => __( 'Primary Button Background Color', CHILD_TEXT_DOMAIN ),
	'section'   => 'colors',
	'default'   => '#E91E63',
	'priority'  => 16,
	'transport' => 'auto',
	'output'    => array(
		array(
			'element'  => '.uk-button-primary'
		,
			'property' => 'background-color'
		),
		array(
			'element'  => 'a:not(.uk-button)'
		,
			'property' => 'color'
		),
	),
) );

wstCustomizer::add_field( 'webstantly_theme', array(
	'type'      => 'color',
	'alpha'     => true,
	'description'=> __('This color will also be used for hover links', CHILD_TEXT_DOMAIN),
	'settings'  => 'primary_button_hover_color',
	'label'     => __( 'Primary Button Background Hover Color', CHILD_TEXT_DOMAIN ),
	'section'   => 'colors',
	'default'   => '#E91E63',
	'priority'  => 16,
	'transport' => 'auto',
	'output'    => array(
		array(
			'element'  => '.uk-button-primary:hover'
		,
			'property' => 'background-color'
		),
		array(
			'element'  => 'a:hover:not(.uk-button)'
		,
			'property' => 'color'
		),
	),
) );

wstCustomizer::add_field( 'webstantly_theme', array(
	'type'      => 'color',
	'alpha'     => true,
	'settings'  => 'secondary_button_color',
	'label'     => __( 'Secondary Button Background Color', CHILD_TEXT_DOMAIN ),
	'section'   => 'colors',
	'default'   => '#2D3047',
	'priority'  => 16,
	'transport' => 'auto',
	'output'    => array(
		array(
			'element'  => '.uk-button-secondary'
		,
			'property' => 'background-color'
		),

	),
) );

wstCustomizer::add_field( 'webstantly_theme', array(
	'type'      => 'color',
	'alpha'     => true,
	'settings'  => 'secondary_button_hover_color',
	'label'     => __( 'Secondary Button Background Hover Color', CHILD_TEXT_DOMAIN ),
	'section'   => 'colors',
	'default'   => '#2D3047',
	'priority'  => 16,
	'transport' => 'auto',
	'output'    => array(
		array(
			'element'  => '.uk-button-secondary:hover'
		,
			'property' => 'background-color'
		),

	),
) );


