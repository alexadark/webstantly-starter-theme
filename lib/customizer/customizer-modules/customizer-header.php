<?php
/*-----------------------------------------------------------
	HEADER
/*------------------------------------------------------------*/

wstCustomizer::add_section( 'header', array(
	'title'      => esc_attr__( 'Header', CHILD_TEXT_DOMAIN ),
	'priority'   => 5,
	'capability' => 'edit_theme_options',
) );

wstCustomizer::add_field( 'webstantly_theme', array(
	'type'        => 'switch',
	'settings'    => 'center_header',
	'label'       => esc_attr__( 'Center Header', CHILD_TEXT_DOMAIN ),
	'description' => esc_attr__( 'If this is enabled logo and menu will be centered, for this use the header right custom menu widget' ),
	'section'     => 'header',
	'choices'     => array(
		'on'  => esc_attr__( 'Enabled', 'textdomain' ),
		'off' => esc_attr__( 'Disabled', 'textdomain' ),
	),
	'default'     => 0,
	'transport'   => 'postMessage',
//	'output'     => array(
//		array(
//			'element' => '.site-header .wrap',
//			'property' => 'flex-direction',
//			'value_pattern' => 'column'
//		),
//		array(
//			'element' => '.site-header .wrap',
//			'property' => 'justify-content',
//			'value_pattern' => 'center'
//		),
//		array(
//			'element' => '.site-header .wrap .header-widget-area',
//			'property' => 'text-align',
//			'value_pattern' => 'center'
//		),
//	)
) );

if ( get_theme_mod( 'center_header', true ) ) {
	add_filter( 'genesis_attr_site-header', 'wst_change_site_header_attr', 99 );
	function wst_change_site_header_attr( $attr ) {
		$attr['class'] .= ' centered';

		return $attr;
	}
}