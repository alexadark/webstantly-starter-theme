<?php
/**
 * Webstantly Starter.
 *
 * This file adds the required WooCommerce setup functions to the Webstantly Starter Theme.
 *
 * @package Webstantly Starter
 * @author  Alexandra Spalato
 * @license GPL-2.0+
 * @link    http://alexandraspalato.com/
 */

add_action( 'wp_enqueue_scripts', 'wst_products_match_height', 99 );
/**
 * Print an inline script to the footer to keep products the same height.
 *
 * @since 2.3.0
 */
function wst_products_match_height() {

	// If Woocommerce is not activated, or a product page isn't showing, exit early.
	if ( ! class_exists( 'WooCommerce' ) || ! is_shop() && ! is_product_category() && ! is_product_tag() && ! is_singular('product') && is_cart() ) {
		return;
	}

	wp_enqueue_script( 'wst-match-height', get_stylesheet_directory_uri() . '/assets/js/jquery.matchHeight.min.js', array( 'jquery' ), CHILD_THEME_VERSION, true );
	wp_enqueue_script( 'wst-set-match-height', get_stylesheet_directory_uri() . '/assets/js/set-matchHeight.js', array( 'wst-match-height' ), CHILD_THEME_VERSION, true );

}

add_filter( 'woocommerce_style_smallscreen_breakpoint', 'wst_woocommerce_breakpoint' );
/**
 * Modify the WooCommerce breakpoints.
 *
 * @since 2.3.0
 *
 * @return string Pixel width of the theme's breakpoint.
 */
function wst_woocommerce_breakpoint() {

	$current = genesis_site_layout();
	$layouts = array(
		'one-sidebar' => array(
			'content-sidebar',
			'sidebar-content',
		),
		'two-sidebar' => array(
			'content-sidebar-sidebar',
			'sidebar-content-sidebar',
			'sidebar-sidebar-content',
		),
	);

	if ( in_array( $current, $layouts['two-sidebar'] ) ) {
		return '2000px'; // Show mobile styles immediately.
	}
	elseif ( in_array( $current, $layouts['one-sidebar'] ) ) {
		return '1200px';
	}
	else {
		return '860px';
	}

}

add_filter( 'genesiswooc_products_per_page', 'wst_default_products_per_page' );
/**
 * Set the default products per page.
 *
 * @since 2.3.0
 *
 * @return int Number of products to show per page.
 */
function wst_default_products_per_page() {
	return 9;
}

add_filter( 'woocommerce_pagination_args', 	'wst_woocommerce_pagination' );
/**
 * Update the next and previous arrows to the default Genesis style.
 *
 * @since 2.3.0
 *
 * @return string New next and previous text string.
 */
function wst_woocommerce_pagination( $args ) {

	$args['prev_text'] = sprintf( '&laquo; %s', __( 'Previous Page', 'genesis-sample' ) );
	$args['next_text'] = sprintf( '%s &raquo;', __( 'Next Page', 'genesis-sample' ) );

	return $args;

}

add_action( 'after_switch_theme', 'wst_woocommerce_image_dimensions_after_theme_setup', 1 );
/**
* Define WooCommerce image sizes on theme activation.
*
* @since 2.3.0
*/
function wst_woocommerce_image_dimensions_after_theme_setup() {

	global $pagenow;

	if ( ! isset( $_GET['activated'] ) || $pagenow != 'themes.php' || ! class_exists( 'WooCommerce' ) ) {
		return;
	}

	wst_update_woocommerce_image_dimensions();

}

add_action( 'activated_plugin', 'wst_woocommerce_image_dimensions_after_woo_activation', 10, 2 );
/**
 * Define the WooCommerce image sizes on WooCommerce activation.
 *
 * @since 2.3.0
 */
function wst_woocommerce_image_dimensions_after_woo_activation( $plugin ) {

	// Check to see if WooCommerce is being activated.
	if ( $plugin !== 'woocommerce/woocommerce.php' ) {
		return;
	}

	wst_update_woocommerce_image_dimensions();

}

/**
 * Update WooCommerce image dimensions.
 *
 * @since 2.3.0
 */
function wst_update_woocommerce_image_dimensions() {

	$catalog = array(
		'width'  => '500', // px
		'height' => '500', // px
		'crop'   => 1,     // true
	);
	$single = array(
		'width'  => '655', // px
		'height' => '655', // px
		'crop'   => 1,     // true
	);
	$thumbnail = array(
		'width'  => '180', // px
		'height' => '180', // px
		'crop'   => 1,     // true
	);

	// Image sizes.
	update_option( 'shop_catalog_image_size', $catalog );     // Product category thumbs.
	update_option( 'shop_single_image_size', $single );       // Single product image.
	update_option( 'shop_thumbnail_image_size', $thumbnail ); // Image gallery thumbs.

}

add_filter( 'woocommerce_enqueue_styles', 'wst_dequeue_wc_styles' );
/**
 * Dequeue WC general styles
 *
 * @since 1.0.0
 *
 * @param $enqueue_styles
 *
 * @return mixed
 */
function wst_dequeue_wc_styles( $enqueue_styles ) {
	unset( $enqueue_styles['woocommerce-general'] );	// Remove the gloss
//	unset( $enqueue_styles['woocommerce-layout'] );		// Remove the layout
//	unset( $enqueue_styles['woocommerce-smallscreen'] );	// Remove the smallscreen optimisation
	return $enqueue_styles;
}


// Add theme support for zoom, lightbox and slider
add_theme_support( 'wc-product-gallery-zoom' );
add_theme_support( 'wc-product-gallery-lightbox' );
add_theme_support( 'wc-product-gallery-slider' );


//Force layout to full width for WC pages
add_filter( 'genesis_site_layout', 'wst_wc_single_fw' );
/**
 * Force layout to full width for single products.
 *
 * @since 1.0.0
 *
 * @return string
 */
function wst_wc_single_fw() {
	if ( 'product' == get_post_type() && is_single() || is_cart() || is_checkout() ) {
		return 'full-width-content';
	}
}


// Change number or products per row to 3
add_filter('loop_shop_columns', 'loop_columns');
if (!function_exists('loop_columns')) {
	function loop_columns() {
		return 3; // 3 products per row
	}
}

//wrap price and rating
add_action( 'woocommerce_after_shop_loop_item_title', 'open_product_info_wrap', 1 );
function open_product_info_wrap(){
	echo '<div class="product-info-wrap">';
}

add_action( 'woocommerce_after_shop_loop_item_title', 'close_product_info_wrap', 15 );
function close_product_info_wrap(){
	echo '</div>';
}


