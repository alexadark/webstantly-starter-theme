<?php
/*-----------------------------------------------------------
HEADER
/*------------------------------------------------------------*/

//logo
if(get_theme_mod('logo_image','')) {
add_filter( 'genesis_seo_title', 'wst_add_span_logo' );
}
function wst_add_span_logo( $html ) {


$site_name = get_bloginfo( 'name' );


$new_html = '<span class="site-logo"><img src=" ' . esc_url(get_theme_mod('logo_image','')) . '" alt=""></span>';


$html = str_replace( $site_name, $new_html, $html );

return $html;

}

//Wrap header

add_action( 'genesis_before_header', 'genesis_wrap_header' );

/**
* Wrap genesis header and make it sticky
*
* @since 1.0.0
*
* @return void
*/
function genesis_wrap_header() {
add_action( 'genesis_before_header', function () { ?>
<div class="header-wrap" >
	<?php }, 15 );
	add_action( 'genesis_after_header', function () {
		echo '</div>';
	}, 15 );
}

add_filter('genesis_attr_site-header','wst_sticky_header_attr',99);
function wst_sticky_header_attr($attr){
	$attr['uk-sticky'] = 'top: 300; animation: uk-animation-slide-top ';
	return $attr;
}

add_action( 'genesis_after_header', 'wst_display_hero_area',6);
function wst_display_hero_area(){


    $image = wp_get_attachment_url( get_post_thumbnail_id() );
    $vimeo = tr_posts_field('vimeo');
    $youtube = tr_posts_field('youtube');
    $height= tr_posts_field('height');
    $bg_class = $vimeo || $youtube ? 'uk-cover-container' : 'uk-background-cover';
    $overlay = tr_posts_field('overlay');
    if(!has_post_thumbnail() && !$youtube && !$vimeo  || !is_singular('page')){
	    return;
    }


    ?>

	<section class="hero-section  <?php echo esc_attr($bg_class) ;  ?>  <?php echo esc_attr( $height);  ?>" <?php if
	(!$youtube &&
	!$vimeo){ ?>style="background: url(<?php echo esc_attr($image); ?>) no-repeat;  " uk-parallax="bgy: -300" <?php } ?>>


	<?php if($youtube) {?>
            <iframe src="//www.youtube.com/embed/<?php echo esc_html($youtube); ?>?autoplay=1&amp;controls=0&amp;showinfo=0&amp;rel=0&amp;loop=1&amp;playlist=<?php echo esc_html($youtube); ?>&amp;modestbranding=1&amp;
            wmode=transparent"
                    width="560"
                    height="315"
                    frameborder="0"
                    allowfullscreen
                    uk-cover></iframe>

                    <?php }  if($vimeo) {?>

            <iframe src="https://player.vimeo.com/video/<?php echo esc_html($vimeo); ?>?autoplay=1&amp;controls=0&amp;showinfo=0&amp;loop=1&amp;playlist=<?php echo esc_html($vimeo); ?>&amp;rel=0&amp;modestbranding=1&amp;wmode=transparent"
                    width="640"
                    height="360"
                    frameborder="0"
                    allowfullscreen
                    uk-cover></iframe>

                    <?php  } ?>

		<div class="<?php echo esc_html($overlay); ?> uk-position-cover ">
		    <div class="uk-position-center">
			<?php if(tr_posts_field('hero_title')) {?>
			<div class="home-heading uk-text-center">
				<h1 class="home-heading-title uk-text-shadow" uk-scrollspy="cls: uk-animation-slide-bottom-small ; delay: 500"><?php echo
					esc_html(tr_posts_field('hero_title'));
					?></h1>
				<?php if(tr_posts_field('hero_subtitle')) {?>
					<div class="home-heading-content uk-text-shadow" uk-scrollspy="cls: uk-animation-slide-bottom-small ; delay: 1000" >
						<?php echo wp_kses_post(tr_posts_field('hero_subtitle')); ?>
					</div>
					<?php $target = tr_posts_field('cta_target') ? 'target="_blank"' : '' ?>
					<?php if(tr_posts_field('cta_text')) {?>
						<a href="<?php echo esc_url(tr_posts_field('cta_url')); ?>" <?php echo $target ?> class="uk-button uk-button-primary uk-margin-top"><?php echo esc_html(tr_posts_field('cta_text')); ?></a>
						<?php
					}
				} ?>
			</div>
            </div>
			<?php
    if('uk-height-viewport' === $height){ ?>

    <a class="uk-position-bottom-center uk-margin-bottom icon-down" href="#firstSection" uk-icon="icon: chevron-down;
    ratio: 2" uk-scroll="offset: 90"></a>
<?php  } ?>
		</div>
		<?php }
		?>
	</section>


<?php
}
