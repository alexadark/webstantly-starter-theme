<?php
/*-----------------------------------------------------------
	GRAVATAR
/*------------------------------------------------------------*/


// Modify size of the Gravatar in the author box.
add_filter( 'genesis_author_box_gravatar_size', 'wst_author_box_gravatar' );
function wst_author_box_gravatar( $size ) {
	return 90;
}

// Modify size of the Gravatar in the entry comments.
add_filter( 'genesis_comment_list_args', 'wst_comments_gravatar' );
function wst_comments_gravatar( $args ) {

	$args['avatar_size'] = 60;

	return $args;

}