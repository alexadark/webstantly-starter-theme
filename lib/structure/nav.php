<?php
/*-----------------------------------------------------------
	NAV
/*------------------------------------------------------------*/

// Rename primary and secondary navigation menus.
add_theme_support( 'genesis-menus', array(
	'primary'   => __( 'After Header Menu', 'genesis-sample' ),
	'secondary' => __( 'Footer Menu', 'genesis-sample' )
) );

// Reposition the secondary navigation menu.
remove_action( 'genesis_after_header', 'genesis_do_subnav' );
add_action( 'genesis_footer', 'genesis_do_subnav', 5 );

// Reduce the secondary navigation menu to one level depth.
add_filter( 'wp_nav_menu_args', 'wst_secondary_menu_args' );
function wst_secondary_menu_args( $args ) {

	if ( 'secondary' != $args['theme_location'] ) {
		return $args;
	}

	$args['depth'] = 1;

	return $args;

}