<?php
/*-----------------------------------------------------------
	POST
/*------------------------------------------------------------*/



remove_action( 'genesis_before_loop', 'genesis_do_posts_page_heading' );


add_filter( 'genesis_site_layout', 'wst_post_force_layout' );
/**
 * Force blog and single layout to content sidebar
 *
 * @since 1.0.0
 *
 * @return string
 */
function wst_post_force_layout() {
	if (  is_singular( 'post' ) ) {
		return 'content-sidebar';
	}

}


add_action( 'wp', 'wst_remove_page_titles' );
function wst_remove_page_titles() {
	if ( ! is_singular( 'page' ) ) {
		return;
	}
	remove_action( 'genesis_entry_header', 'genesis_do_post_title' );
}

//Reposition featured image in archive.

remove_action( 'genesis_entry_content', 'genesis_do_post_image', 8 );

add_action( 'genesis_before_entry', 'genesis_do_post_image' );

add_action( 'genesis_before_entry', 'wst_show_featured_image_single_post' );



/**
 * Display featured image (if present) before entry on single Posts
 */

function wst_show_featured_image_single_post() {
	if ( ! ( is_singular( 'post' ) && has_post_thumbnail() ) ) {
		return;
	}

	$args = array(
		'size' => 'featured-image',
		'attr' => array(
			'class' => 'featured-image',
		),
	);
	genesis_image( $args );

}
