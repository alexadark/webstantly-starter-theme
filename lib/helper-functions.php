<?php
/**
 * Webstantly Starter.
 *
 * This file adds the required helper functions used in the Webstantly Starter Theme.
 *
 * @package Webstantly Starter
 * @author  Alexandra Spalato
 * @license GPL-2.0+
 * @link    http://alexandraspalato.com/
 */

/**
 * Get default link color for Customizer.
 * Abstracted here since at least two functions use it.
 *
 * @since 1.0.0
 *
 * @return string Hex color code for link color.
 */
function wst_customizer_get_default_link_color() {
	return '#c3251d';
}

/**
 * Get default accent color for Customizer.
 * Abstracted here since at least two functions use it.
 *
 * @since 1.0.0
 *
 * @return string Hex color code for accent color.
 */
function wst_customizer_get_default_accent_color() {
	return '#c3251d';
}

/**
 * Calculate the color contrast.
 *
 * @since 1.0.0
 *
 * @return string Hex color code for contrast color
 */
function wst_color_contrast( $color ) {

	$hexcolor = str_replace( '#', '', $color );
	$red      = hexdec( substr( $hexcolor, 0, 2 ) );
	$green    = hexdec( substr( $hexcolor, 2, 2 ) );
	$blue     = hexdec( substr( $hexcolor, 4, 2 ) );

	$luminosity = ( ( $red * 0.2126 ) + ( $green * 0.7152 ) + ( $blue * 0.0722 ) );

	return ( $luminosity > 128 ) ? '#333333' : '#ffffff';

}

/**
 * Calculate the color brightness.
 *
 * @since 1.0.0
 *
 * @return string Hex color code for the color brightness
 */
function wst_color_brightness( $color, $change ) {

	$hexcolor = str_replace( '#', '', $color );

	$red   = hexdec( substr( $hexcolor, 0, 2 ) );
	$green = hexdec( substr( $hexcolor, 2, 2 ) );
	$blue  = hexdec( substr( $hexcolor, 4, 2 ) );

	$red   = max( 0, min( 255, $red + $change ) );
	$green = max( 0, min( 255, $green + $change ) );
	$blue  = max( 0, min( 255, $blue + $change ) );

	return '#' . dechex( $red ) . dechex( $green ) . dechex( $blue );

}

function wst_count_widgets( $id ) {

	global $sidebars_widgets;

	if ( isset( $sidebars_widgets[ $id ] ) ) {
		return count( $sidebars_widgets[ $id ] );
	}

}

/**
 * Get the class string for a flexible widget. (works with uikit)
 *
 * @since 1.0.0
 *
 * @param $id
 *
 * @return string
 */
function wst_widget_area_class( $id ) {

	$count = wst_count_widgets( $id );

	$class = '';

	if ( 1 === $count ) {
		$class .= ' uk-child-width-1-1';
	} elseif ( 0 === $count % 3 ) {
		$class .= ' uk-child-width-1-3@m';
	} elseif ( 0 === $count % 4 ) {
		$class .= ' uk-child-width-1-4@m';
	} elseif ( 1 === $count % 2 && 7 !== $count ) {
		$class .= ' uk-child-width-1-2@m uneven';
	} elseif ( 0 === $count % 7 ) {
		$class .= ' uk-child-width-1-3@m uneven';
	} else {
		$class .= ' uk-child-width-1-2@m';
	}

	return $class;

}

/**
 * Add body class if featured image exist
 *
 * @since 1.0.0
 *
 * @param $classes
 *
 * @return array
 */
function wst_add_hero_body_class( $classes ) {
	global $post;
	$vimeo   = tr_posts_field( 'vimeo' );
	$youtube = tr_posts_field( 'youtube' );
	if ( isset ( $post->ID ) && ( get_the_post_thumbnail( $post->ID ) || $vimeo || $youtube ) && is_singular( 'page' ) ) {
		$classes[] = 'has-hero-area';
	}

	return $classes;
}

add_filter( 'body_class', 'wst_add_hero_body_class' );


add_filter( 'timber_context', 'wst_add_to_context' );
/**
 * add post to global context
 *
 * @since 1.0.0
 *
 * @param $context
 *
 * @return mixed
 */
function wst_add_to_context( $context ) {
	$post            = new TimberPost();
	$context['post'] = $post;

	return $context;
}













