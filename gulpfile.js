'use strict';
var gulp = require('gulp'),

    //Sass/css processes
    sass = require('gulp-sass'),
    postcss = require('gulp-postcss'),
    autoprefixer = require('autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    mqpacker = require("css-mqpacker"),
    cssMinify = require('gulp-cssnano'),
    purifycss = require("purify-css"),

    //utilities
    rename = require('gulp-rename'),
    notify = require('gulp-notify'),
    plumber = require('gulp-plumber'),
    sassLint = require('gulp-sass-lint'),
    browserSync = require('browser-sync');

/*-----------------------------------------------------------
	UTILITIES
/*------------------------------------------------------------*/

//Error handling

 function handleErrors(){
     var args = Array.prototype.slice.call(arguments);

     notify.onError({
         title: 'Task Failed [<%= error.message %>',
         message: 'See console.',
         sound: 'Sosumi' // See: https://github.com/mikaelbr/node-notifier#all-notification-options-with-their-defaults
     }).apply(this, args);

     gutil.beep(); // Beep 'sosumi' again

     // Prevent the 'watch' task from stopping
     this.emit('end');
 }

/*-----------------------------------------------------------
	CSS TASKS
/*------------------------------------------------------------*/

gulp.task('styles',['sassLint'], function () {
    return gulp.src('assets/sass/style.scss')

        //Error handling
        .pipe(plumber({
            errorHandler: handleErrors
        }))
    //wrap task in a sourcemap
        .pipe(sourcemaps.init())
        .pipe(sass({
            errLogToConsole: true,
            outputStyle: 'expanded'
        }))
        .pipe( postcss([
            autoprefixer({
                browsers: ['last 2 versions']
            }),
            // mqpacker({
            //     sort: true
            // })
        ]))

        // creates the sourcemap
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./'))
        .pipe(browserSync.stream())
        .pipe(notify({
            message: 'Styles are built'
        }));

});

gulp.task('cssMinify',['styles'], function(){
    return gulp.src('style.css')
    //Error handling
        .pipe(plumber({
            errorHandler: handleErrors
        }))
        .pipe(cssMinify({
            safe: true
        }))
        .pipe(rename('style.min.css'))
        .pipe(gulp.dest('./'))
        .pipe(notify({
            message: 'Styles are minified'
        }));
});

gulp.task('sassLint', function(){
    gulp.src([
        'assets/sass/style.scss',
        '!assets/sass/uikit3/**/*.*'

    ])
        .pipe(sassLint())
        .pipe(sassLint.format())
        .pipe(sassLint.failOnError())
});

/*-----------------------------------------------------------
 CALL TASKS
 /*------------------------------------------------------------*/


gulp.task('watch', function () {
    // Kick off BrowserSync.
    browserSync.init({
        proxy: "webstantly.dev"
    });
    gulp.watch('assets/sass/**/*.scss', ['styles']);
    gulp.watch('./**/*.php',browserSync.reload);
});


gulp.task('mini',['cssMinify']);
