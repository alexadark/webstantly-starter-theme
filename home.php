<?php
$layout = get_theme_mod('blog_layout');

add_filter( 'genesis_site_layout', 'wst_post_blog_layout' );
/**
 * Force blog to layout choosen in the customizer
 *
 * @since 1.0.0
 *
 * @return string
 */
function wst_post_blog_layout() {
	global $layout;

	if('content_sidebar' === $layout) {
		return 'content-sidebar';
	} elseif ('sidebar_content' === $layout){
		return 'sidebar-content';
	} else {
		return 'full-width-content';
	}

}


//wrap articles
add_action( 'genesis_before_entry', 'genesis_wrap_articles' , 1);
/**
 * wrap image and article in order to make a grid
 *
 *@since 1.0.0
 *
 *@return void
 */
function genesis_wrap_articles(){
//	global $layout;
//	if($layout === 'content_sidebar' || 'sidebar_content'){
//		return;
//	}

	add_action('genesis_before_entry', function(){
			echo '<div class="article-wrap" > ';
	},5);
	add_action('genesis_after_entry', function(){
		echo '</div>';
	},15);

}

//add_filter('genesis_attr_content','wst_change_content_attr',99);
//function wst_change_content_attr($attr){
//$attr['class'] .= ' uk-child-width-1-2@m';
//$attr['uk-grid'] = ' ';
//return $attr;
//}




genesis();