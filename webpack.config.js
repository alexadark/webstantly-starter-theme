var path = require('path');

module.exports = {
    entry: './assets/js/uikit/uikit.js',
    output: {
        filename: 'uikit-bundle.js',
        path: path.resolve(__dirname, './assets/js/uikit/')
    }
};