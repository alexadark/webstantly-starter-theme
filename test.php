<?php
// Template name: test page
add_action( 'genesis_entry_content', 'wst_test_uikit' );
function wst_test_uikit() {
	?>

    <a href="" uk-icon="icon: heart_red; ratio:2">Heart</a>

    <form>
        <fieldset class="uk-fieldset">

            <legend class="uk-legend">Legend</legend>

            <div class="uk-margin">
                <input class="uk-input" type="text" placeholder="Input">
            </div>

            <div class="uk-margin">
                <select class="uk-select">
                    <option>Option 01</option>
                    <option>Option 02</option>
                </select>
            </div>

            <div class="uk-margin">
                <textarea class="uk-textarea" rows="5" placeholder="Textarea"></textarea>
            </div>

            <div class="uk-margin uk-grid-small uk-child-width-auto uk-grid">
                <label><input class="uk-radio" type="radio" name="radio2" checked> A</label>
                <label><input class="uk-radio" type="radio" name="radio2"> B</label>
            </div>

            <div class="uk-margin uk-grid-small uk-child-width-auto uk-grid">
                <label><input class="uk-checkbox" type="checkbox" checked> A</label>
                <label><input class="uk-checkbox" type="checkbox"> B</label>
            </div>

            <div class="uk-margin">
                <input class="uk-range" type="range" value="2" min="0" max="10" step="0.1">
            </div>

        </fieldset>
    </form>

<h2>BACKGROUNDS</h2>
    <div class="uk-child-width-1-2@s uk-text-center" uk-grid>
        <div>
            <div class="uk-background-default uk-padding uk-panel">
                <p class="uk-h4">Default</p>
            </div>
        </div>
        <div>
            <div class="uk-background-muted uk-padding uk-panel">
                <p class="uk-h4">Muted</p>
            </div>
        </div>
        <div>
            <div class="uk-background-primary uk-light uk-padding uk-panel">
                <p class="uk-h4">Primary</p>
            </div>
        </div>
        <div>
            <div class="uk-background-secondary uk-light uk-padding uk-panel">
                <p class="uk-h4">Secondary</p>
            </div>
        </div>
    </div>



    <div class="uk-child-width-1-2@s uk-light" uk-grid>
        <div>
            <div class="uk-background-cover uk-height-large uk-panel uk-flex uk-flex-center uk-flex-middle uk-background-center-center" style="background-image: url(https://getuikit.com/docs/images/dark.jpg);">
                <p class="uk-h4">Cover</p>
            </div>
        </div>
        <div>
            <div class="uk-background-contain uk-background-muted uk-height-large uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(https://getuikit.com/docs/images/dark.jpg);">
                <p class="uk-h4">Contain</p>
            </div>
        </div>
    </div>

    <div class="uk-background-fixed uk-background-center-center uk-height-medium uk-width-large uk-margin-large" style="background-image: url(https://getuikit.com/docs/images/dark.jpg);"></div>


    <div class="uk-child-width-1-2 uk-child-width-1-3@s uk-grid-small uk-light" uk-grid>
        <div>
            <div class="uk-background-blend-multiply uk-background-cover uk-height-small
            uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image:  url(https://getuikit.com/docs/images/dark.jpg); background-color: olive;">
                <p class="uk-h4">Multiply</p>
            </div>
        </div>
        <div>
            <div class="uk-background-blend-screen uk-background-primary uk-background-cover uk-height-small uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(https://getuikit.com/docs/images/dark.jpg);">
                <p class="uk-h4">Screen</p>
            </div>
        </div>
        <div>
            <div class="uk-background-blend-overlay uk-background-primary uk-background-cover uk-height-small uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(https://getuikit.com/docs/images/dark.jpg);">
                <p class="uk-h4">Overlay</p>
            </div>
        </div>
        <div>
            <div class="uk-background-blend-darken uk-background-primary uk-background-cover uk-height-small uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(https://getuikit.com/docs/images/dark.jpg);">
                <p class="uk-h4">Darken</p>
            </div>
        </div>
        <div>
            <div class="uk-background-blend-lighten uk-background-primary uk-background-cover uk-height-small uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(https://getuikit.com/docs/images/dark.jpg);">
                <p class="uk-h4">Lighten</p>
            </div>
        </div>
        <div>
            <div class="uk-background-blend-color-dodge uk-background-primary uk-background-cover uk-height-small uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(https://getuikit.com/docs/images/dark.jpg);">
                <p class="uk-h4">Color Dodge</p>
            </div>
        </div>
        <div>
            <div class="uk-background-blend-color-burn uk-background-primary uk-background-cover uk-height-small uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(https://getuikit.com/docs/images/dark.jpg);">
                <p class="uk-h4">Color Burn</p>
            </div>
        </div>
        <div>
            <div class="uk-background-blend-hard-light uk-background-primary uk-background-cover uk-height-small uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(https://getuikit.com/docs/images/dark.jpg);">
                <p class="uk-h4">Hard Light</p>
            </div>
        </div>
        <div>
            <div class="uk-background-blend-soft-light uk-background-primary uk-background-cover uk-height-small uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(https://getuikit.com/docs/images/dark.jpg);">
                <p class="uk-h4">Soft Light</p>
            </div>
        </div>
        <div>
            <div class="uk-background-blend-difference uk-background-primary uk-background-cover uk-height-small uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(https://getuikit.com/docs/images/dark.jpg);">
                <p class="uk-h4">Difference</p>
            </div>
        </div>
        <div>
            <div class="uk-background-blend-exclusion uk-background-primary uk-background-cover uk-height-small uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(https://getuikit.com/docs/images/dark.jpg);">
                <p class="uk-h4">Exclusion</p>
            </div>
        </div>
        <div>
            <div class="uk-background-blend-hue uk-background-primary uk-background-cover uk-height-small uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(https://getuikit.com/docs/images/dark.jpg);">
                <p class="uk-h4">Hue</p>
            </div>
        </div>
        <div>
            <div class="uk-background-blend-saturation uk-background-primary uk-background-cover uk-height-small uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(https://getuikit.com/docs/images/dark.jpg);">
                <p class="uk-h4">Saturation</p>
            </div>
        </div>
        <div>
            <div class="uk-background-blend-color uk-background-primary uk-background-cover uk-height-small uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(https://getuikit.com/docs/images/dark.jpg);">
                <p class="uk-h4">Color</p>
            </div>
        </div>
        <div>
            <div class="uk-background-blend-luminosity uk-background-primary uk-background-cover uk-height-small uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(https://getuikit.com/docs/images/dark.jpg);">
                <p class="uk-h4">Luminosity</p>
            </div>
        </div>
    </div>

    <div class="uk-width-large@m uk-width-small uk-margin-large">
        <img src="https://getuikit.com/docs/images/dark.jpg" alt="Image">
    </div>

    <blockquote cite="#">
        <p class="uk-margin-small-bottom">The blockquote element represents content that is quoted from another source, optionally with a citation which must be within a footer or cite element.</p>
        <footer>Someone famous in <cite><a href="#">Source Title</a></cite></footer>
    </blockquote>

    <button class="uk-button uk-button-default uk-width-1-3 uk-margin-small-bottom uk-align-center">Button</button>
    <button class="uk-button uk-button-primary uk-width-1-1 uk-margin-small-bottom">Button</button>
    <button class="uk-button uk-button-secondary uk-width-1-1">Button</button>

    <div class="uk-child-width-1-2@m uk-margin-large-top" uk-grid>
        <div>
            <div class="uk-card uk-card-default">
                <div class="uk-card-media-top">
                    <img src="https://getuikit.com/docs/images/light.jpg" alt="">
                </div>
                <div class="uk-card-body">
                    <h3 class="uk-card-title">Media Top</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
                </div>
            </div>
        </div>
        <div>
            <div class="uk-card uk-card-default">
                <div class="uk-card-body">
                    <h3 class="uk-card-title">Media Bottom</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
                </div>
                <div class="uk-card-media-bottom">
                    <img src="https://getuikit.com/docs/images/light.jpg" alt="">
                </div>
            </div>
        </div>
    </div>

    <h1>Icons</h1>

    <span class="uk-margin-small-right" uk-icon="icon: check"></span>

    <a href="" uk-icon="icon: heart"></a>

    <ul uk-accordion>
        <li class="uk-open">
            <h3 class="uk-accordion-title">Item 1</h3>
            <div class="uk-accordion-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            </div>
        </li>
        <li>
            <h3 class="uk-accordion-title">Item 2</h3>
            <div class="uk-accordion-content">
                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor reprehenderit.</p>
            </div>
        </li>
        <li>
            <h3 class="uk-accordion-title">Item 3</h3>
            <div class="uk-accordion-content">
                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat proident.</p>
            </div>
        </li>
    </ul>

    <div class="uk-height-large uk-background-cover uk-light uk-flex" uk-parallax="bgy: -200" style="background-image: url('https://getuikit.com/docs/images/dark.jpg');">

        <h1 class="uk-width-1-2@m uk-text-center uk-margin-auto uk-margin-auto-vertical">Headline</h1>

    </div>


	<?php
//	$context   = Timber::get_context();
//	$templates = array( 'test.twig' );
//	Timber::render( $templates, $context );



}



genesis();
