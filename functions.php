<?php
/**
 * Webstantly starter.
 *
 * This file adds functions to the Genesis Webstantly Starter Theme.
 *
 * @package Webstantly starter
 * @author  Alexandra Spalato
 * @license GPL-2.0+
 * @link    http://alexandraspalato.com/
 */

// Start the engine.
include_once( get_template_directory() . '/lib/init.php' );

//Initialize theme constants

$child_theme = wp_get_theme();

define( 'CHILD_THEME_NAME', $child_theme->get( 'Name' ) );
define( 'CHILD_THEME_URL', $child_theme->get( 'ThemeURI' ) );
define( 'CHILD_THEME_VERSION', $child_theme->get( 'Version' ) );
define( 'CHILD_TEXT_DOMAIN', $child_theme->get( 'TextDomain' ) );

define( 'CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'CHILD_URI', get_stylesheet_directory_uri() );
define( 'CHILD_CONFIG_DIR', CHILD_THEME_DIR . '/config/' );
define( 'CHILD_LIB', CHILD_THEME_DIR . '/lib/' );
define( 'CHILD_IMG', CHILD_URI . '/assets/images/' );
define( 'CHILD_JS', CHILD_URI . '/assets/js/' );


/*-----------------------------------------------------------
	LOAD ASSETS
/*------------------------------------------------------------*/

// Enqueue Scripts and Styles.
add_action( 'wp_enqueue_scripts', 'wst_enqueue_scripts_styles' );
function wst_enqueue_scripts_styles() {

	wp_enqueue_style( 'genesis-sample-fonts', '//fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700', array(), CHILD_THEME_VERSION );
	wp_enqueue_style( 'dashicons' );
	wp_enqueue_script( 'uikit-js', CHILD_JS . 'uikit/uikit-bundle.js', array( 'jquery' ), '3.0.0-beta.30', true );
	wp_enqueue_script( 'theme-js', CHILD_JS . 'theme.js', array(
		'jquery',
	), CHILD_THEME_VERSION, true );

	$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';
	wp_enqueue_script( 'genesis-sample-responsive-menu', CHILD_JS . "responsive-menus{$suffix}.js", array( 'jquery' ), CHILD_THEME_VERSION, true );
	wp_localize_script(
		'genesis-sample-responsive-menu',
		'genesis_responsive_menu',
		wst_responsive_menu_settings()
	);

}

add_action( 'customize_preview_init', 'wst_customize_preview_js' );
/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function wst_customize_preview_js() {
	wp_enqueue_script( 'wst-customizer', get_stylesheet_directory_uri() . '/assets/js/theme-customizer.js', array( 'customize-preview' ), '1', true );
}


// Define our responsive menu settings.
function wst_responsive_menu_settings() {

	$settings = array(
		'mainMenu'          => __( 'Menu', 'genesis-sample' ),
		'menuIconClass'     => 'dashicons-before dashicons-menu',
		'subMenu'           => __( 'Submenu', 'genesis-sample' ),
		'subMenuIconsClass' => 'dashicons-before dashicons-arrow-down-alt2',
		'menuClasses'       => array(
			'combine' => array(
				'.nav-primary',
				'.nav-header',
			),
			'others'  => array(),
		),
	);

	return $settings;

}

// Change order of main stylesheet to override plugin styles.
remove_action( 'genesis_meta', 'genesis_load_stylesheet' );
add_action( 'wp_enqueue_scripts', 'genesis_enqueue_main_stylesheet', 99 );


// Setup Theme.
include_once( CHILD_THEME_DIR . '/lib/theme-defaults.php' );

// Set Localization (do not remove).
add_action( 'after_setup_theme', 'wst_localization_setup' );
function wst_localization_setup() {
	load_child_theme_textdomain( CHILD_TEXT_DOMAIN, CHILD_THEME_DIR . 'assets/languages' );
}


// Add the helper functions.
include_once( CHILD_THEME_DIR . '/lib/helper-functions.php' );

// Add WooCommerce support.
include_once( CHILD_THEME_DIR . '/lib/woocommerce-setup.php' );


//Customizer options




// Include Kirki custom controls.
require_once( CHILD_THEME_DIR . '/lib/customizer/include-kirki.php' );


// Include Kirki fallback.
require_once( CHILD_THEME_DIR . '/lib/customizer/kirki-fallback.php' );


//  WordPress Theme Customizer.
require_once( CHILD_THEME_DIR . '/lib/customizer/customizer.php' );

// Include Customizer CSS.
include_once( get_stylesheet_directory() . '/lib/customizer/output.php' );





/*-----------------------------------------------------------
	THEME SUPPORTS
/*------------------------------------------------------------*/

// Add HTML5 markup structure.
add_theme_support( 'html5', array( 'caption', 'comment-form', 'comment-list', 'gallery', 'search-form' ) );

// Add Accessibility support.
add_theme_support( 'genesis-accessibility', array(
	'404-page',
	'drop-down-menu',
	'headings',
	'rems',
	'search-form',
	'skip-links'
) );

// Add viewport meta tag for mobile browsers.
add_theme_support( 'genesis-responsive-viewport' );

// Add support for custom header.
//add_theme_support( 'custom-header', array(
//	'width'           => 150,
//	'height'          => 150,
//	'header-selector' => '.site-title a',
//	'header-text'     => false,
//	'flex-height'     => true,
//) );

// Add support for custom background.
add_theme_support( 'custom-background' );

// Add support for after entry widget.
add_theme_support( 'genesis-after-entry-widget-area' );

// Add support for 3-column footer widgets.
//add_theme_support( 'genesis-footer-widgets', 3 );

//add support for structural wraps
add_theme_support( 'genesis-structural-wraps', array(
	'site-inner',
	'header',
	'nav',
	'subnav',
	'footer-widgets',
	'footer'
) );

/*-----------------------------------------------------------
	REMOVE SITE LAYOUTS
/*------------------------------------------------------------*/

genesis_unregister_layout( 'content-sidebar-sidebar' );
genesis_unregister_layout( 'sidebar-content-sidebar' );
genesis_unregister_layout( 'sidebar-sidebar-content' );


/*-----------------------------------------------------------
	IMAGE SIZES
/*------------------------------------------------------------*/

add_image_size( 'featured-image', 1000, 400, true );
/*-----------------------------------------------------------
	WIDGET AREAS
/*------------------------------------------------------------*/
genesis_register_widget_area(
	array(
		'id'          => 'flex-footer',
		'name'        => __( 'Flexible footer', CHILD_TEXT_DOMAIN ),
		'description' => __( 'This is the footer flexible widgets section', CHILD_TEXT_DOMAIN ),
	)
);


include_once 'lib/structure/nav.php';

include_once 'lib/structure/gravatar.php';

include_once 'lib/structure/header.php';
include_once 'lib/structure/post.php';
include_once 'lib/structure/footer.php';





